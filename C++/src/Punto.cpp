#include "Punto.hpp"

namespace PuntoNamespace {

  Punto::Punto(const double& x,const double& y)
  {
    _x = x;
    _y = y;

  }

  Punto Punto::SmallestPoint(vector<Punto> vertices)
  {
          Punto small;
          int min=0;
          for(unsigned int i=1; i<vertices.size(); i++){
              if(vertices[i]._x < vertices[min]._x) min=i;
              else if(abs(vertices[i]._x - vertices[min]._x) < 1e-8)
                  {
                    if(vertices[i]._y < vertices[min]._y) min = i;
                  }
              }
          small._x = vertices[min]._x;
          small._y = vertices[min]._y;
          return small;
  }

  Punto Punto::BiggestPoint(vector<Punto> vertices)
  {
      Punto big;
      int max=0;
      for(unsigned int i=1; i<vertices.size(); i++){
          if(vertices[i]._x > vertices[max]._x) max=i;
          else if(abs(vertices[i]._x - vertices[max]._x) < 1e-8)
              {
                if(vertices[i]._y > vertices[max]._y) max = i;
              }
          }
      big._x = vertices[max]._x;
      big._y = vertices[max]._y;
      return big;

  }


}
