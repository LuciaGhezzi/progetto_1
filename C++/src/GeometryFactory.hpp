#ifndef GEOMETRYFACTORY_H
#define GEOMETRYFACTORY_H

#include "Eigen"
#include "unordered_map"
#include "Punto.hpp"
#include "Segment.hpp"
#include "iostream"

#include <vector>
#include <list>
#include <map>
using namespace Eigen;
using namespace std;
using namespace PuntoNamespace;
using namespace SegmentNamespace;

namespace GeometryFactoryNamespace {

  class GeometryFactory {
    public:
      vector<Punto> _polygonVertices; //vertici del poligono
      vector<Segment> _polygonEdges;//lati del poligono
      Segment _intersectionSegment;//segmento s1-s2

      unordered_map<int, Punto> _intersectionPoints;//(chiave:lato di intersezione,valore:coordinate del punto corrispondente)
      list<Punto> _sortedPoints;//punti della retta(s1,s2+intersezioni) ordinati per x crescente
      list<list<int>> _cuttedPolygons;//lista contenente gli insiemi formati dai vertici dei poligoni tagliati
      vector<Punto> _newPoints;//vettore che contiene tutti i punti del poligono(vertici+ intersezioni+s1+s2)

      bool SetPolygon(const vector<Vector2d>& points);//creo poligono
      bool SetSegment(const vector<Vector2d>& segment);//creo segmento s1-s2
      void SortPoints();//ordino i punti per x crescente
      void findIntersection();//trova le intersezioni
      void fillNewPoints();//riempe il vettore di tutti i punti(newpoints)

      //taglio dei poligoni
      pair <vector<Punto>,list<list<int>>> CutPolygon(const vector<Vector2d>& points, const vector<Vector2d>& segment, const vector<int>& polygonVertices);



  };
}

#endif // GEOMETRYFACTORY_H
