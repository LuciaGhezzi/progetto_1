#ifndef PUNTO_H
#define PUNTO_H
#include "math.h"
#include "Eigen"
#include "unordered_map"
#include "iostream"
#include <vector>
#include <list>
#include <map>
#include "unordered_map"
using namespace std;
namespace PuntoNamespace {
  class Punto {
    public:
      double _x;
      double _y;
      Punto(){};
      ~Punto(){};


      Punto(const double& x,const double& y);

      //SmallestPoint:ricerca del punto con x minima
      static Punto SmallestPoint(vector<Punto> vertices);

      //BiggestPoint:ricerca del punto con x massima
      static Punto BiggestPoint(vector<Punto> vertices);

      //Equal: confronta l'uguaglianza di due punti
      static bool equal(const Punto& p1,const Punto& p2){
          if((abs(p1._x - p2._x )< 1e-8) && (abs(p1._y - p2._y) < 1e-8)){
              return true;
          }
          return false;
      }

  };
}

#endif // PUNTO_H
