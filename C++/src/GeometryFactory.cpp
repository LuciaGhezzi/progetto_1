#include "GeometryFactory.hpp"

namespace GeometryFactoryNamespace {

bool GeometryFactory::SetPolygon(const vector<Vector2d>& points)
{
    //conto vertici del poligono
    unsigned int numVertices = points.size();

    //controllo che i vertici siano >3
    if( numVertices < 3){
        throw runtime_error("errore nel creare poligono");
        return false;
    }


    //creo vettori di grandezza il numero dei vertici
    _polygonVertices.reserve(numVertices);
    _polygonEdges.reserve(numVertices);

    //riempo vettore con i vertici
    for(unsigned int i=0; i<numVertices; i++){
        const Vector2d& vertex = points[i];
        Punto punto;
        punto._x = vertex(0);
        punto._y = vertex(1);
        _polygonVertices.push_back(punto);
    }

    //riempo vettori con i lati(ogni elemento è un segmento(from,to))
    for(unsigned int i=0; i<numVertices; i++){
        Segment segment;
        segment._from = _polygonVertices[i];
        segment._to = _polygonVertices[(i+1)%numVertices];
        _polygonEdges.push_back(segment);
    }

    return true;

}

bool GeometryFactory::SetSegment(const vector<Vector2d>& segment) //Definisco segmento
{
    //assegno i valori punto da e punto per
    const Punto& puntoda = Punto(segment[0](0), segment[0](1));
    const Punto& puntoper = Punto(segment[1](0), segment[1](1));
    const  Segment& segmento = Segment(puntoda, puntoper);
    _intersectionSegment = segmento;

    //controllo se ho passato due punti al segmento
    if(segment.size() != 2){
        throw runtime_error("errore nel creare poligono");
        return false;
    }
    return true;
}

void GeometryFactory::SortPoints()
{
    //creo vettore di supporto di dimensione (intersezioni+s1+s2)
    vector<Punto> punti;
    punti.reserve(_intersectionPoints.size() + 2);

    //inserisco punto from e punto to del segmento
    punti.push_back(_intersectionSegment._from);
    punti.push_back(_intersectionSegment._to);

    //scorro la lista e inserisco tutti i punti di intersezione
    for(auto iter=_intersectionPoints.begin(); iter!=_intersectionPoints.end();iter++){
        Punto punto = iter->second;
        punti.push_back(punto);
    }

    //ordino il vettore "punti"(vettore di supporto)tramite Smallestpoint
    for(unsigned int i=0; i<punti.size(); i++){
        Punto piccolo = Punto::SmallestPoint(punti);
        _sortedPoints.push_back(piccolo);

        //tolgo i vertici inseriti da punti
        for(unsigned int i=0; i<punti.size(); i++){

            //assegno il massimo dei punti(rispetto a x) tramite Biggest
            if(Punto::equal(piccolo, punti[i]) == true)
                punti[i] = Punto::BiggestPoint(punti);
        }

    }

}

void GeometryFactory::findIntersection()
{
    Matrix2d matrixTangentVector;
    Vector2d originFirstSegment, rightHandSide, endFirstSegment;

    //Definisco il primo segmento
    matrixTangentVector.col(0) << _intersectionSegment._to._x - _intersectionSegment._from._x, _intersectionSegment._to._y - _intersectionSegment._from._y;
    originFirstSegment << _intersectionSegment._from._x, _intersectionSegment._from._y;
    endFirstSegment << _intersectionSegment._to._x, _intersectionSegment._to._y;

    //scorro su tutti i lati per cercare l'intersezione
    for(unsigned int i=0; i<_polygonEdges.size(); i++){

        //Definisco secondo segmento
        matrixTangentVector.col(1) << _polygonEdges[i]._from._x - _polygonEdges[i]._to._x , _polygonEdges[i]._from._y - _polygonEdges[i]._to._y;
        rightHandSide << _polygonEdges[i]._from._x - originFirstSegment(0),_polygonEdges[i]._from._y - originFirstSegment(1);

        //calcolo intersezione: faccio inversa(matrixTangentVector)
        Matrix2d solverMatrix;
        solverMatrix << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0);

        // <li> Se il lato e la traccia della matrice non sono paralleli  cerco l'intersezione tramite la coordinata parametrica
        Vector2d resultParametricCoordinates = solverMatrix * rightHandSide;

        //nell'inversa devo dividere per il determinante della matrice, lo faccio ora così faccio solo una divisione
        resultParametricCoordinates /= matrixTangentVector.determinant();

        //tipologia intersezione:
        if ( resultParametricCoordinates(1) >= 1e-8   && (resultParametricCoordinates(1)-1.0) <= 1e-8 )
        {
            Punto intersectionPoint;
            intersectionPoint._x = _polygonEdges[i]._from._x + (_polygonEdges[i]._to._x - _polygonEdges[i]._from._x)*resultParametricCoordinates(1);
            intersectionPoint._y = _polygonEdges[i]._from._y + (_polygonEdges[i]._to._y - _polygonEdges[i]._from._y)*resultParametricCoordinates(1);
            _intersectionPoints.insert(pair<int,Punto>(i,intersectionPoint));
            //se è un vertice eliminalo
            if( Punto::equal(intersectionPoint,_polygonEdges[i]._from)==true ||Punto::equal(intersectionPoint,_polygonEdges[i]._to)==true){
                //controllo se c'è un vertice tra le intersezioni per tutti i lati
                if(i>=1 && Punto::equal(_intersectionPoints.at(i),_intersectionPoints.at(i-1))==true ) {
                    _intersectionPoints.erase(i);
                }

                if(i==(_polygonEdges.size() -1 ) && Punto::equal(_intersectionPoints.at(i),_intersectionPoints.at(0))==true ) {
                    _intersectionPoints.erase(i);
                }
            }




        }
    }
}



void GeometryFactory::fillNewPoints()
{
    unsigned int i=0;
    int ind;

    //aggiungo vertici
    while(i<_polygonVertices.size())
    {
        _newPoints.push_back(_polygonVertices[i]);
        // _MapNewPoints.insert(pair<int, Punto>(i,_polygonVertices[i]));
        ind=i;
        i++;
    }

    //scorro la retta ordinata e inserisco tutti i punti
    for(list<Punto>::iterator j=_sortedPoints.begin(); j!=_sortedPoints.end(); j++){
        Punto& point = *j;
        unsigned int flag=0;

        //controllo che il punto non sia un vertice
        for(unsigned int i=0; i<_polygonVertices.size();i++){
            if(Punto::equal(point, _polygonVertices[i]) == true) flag = 1;
        }

        //se non è vertice lo inserisco
        if(flag==0){
            _newPoints.push_back(point);
            //_MapNewPoints.insert(pair<int,Punto>(ind+1,point));
            ind++;
        }
    }
}


pair<vector<Punto>, list<list<int>> > GeometryFactory::CutPolygon(const vector<Vector2d>& points, const vector<Vector2d>& segment, const vector<int>& polygonVertices)
{
    unsigned int all_used=0;
    list<int> vertici;
    map<int, list<Punto>> _polygons; //riguardare


    //salvo s1, s2 (estremi del  segmento) in due variabili
    Punto s1 = Punto(segment[0](0),segment[0](1));
    Punto s2 = Punto(segment[1](0),segment[1](1));

    //salvo l'ultimo punto della retta ordinata
    Punto sorted_end = _sortedPoints.back();

    //inizializzo il numero di poligoni partendo da 1
    unsigned int num_polygon = 1;

    //lista di supporto dei poligoni tagliati ad ogni giro
    list<Punto> cutPoints;

    //riempo la lista di supporto dei vertici
    vector<Punto> verticesSupp;
    verticesSupp.reserve(points.size());
    for(unsigned int i=0; i<points.size(); i++){
        const Vector2d& vertex = points[i];
        Punto punto;
        punto._x = vertex(0);
        punto._y = vertex(1);
        verticesSupp.push_back(punto);
    }

    //prendo il punto con x massima tramite Biggest
    Punto fine =Punto::BiggestPoint(verticesSupp);


    do{
        //parto dal vertice con x più piccola(tramite Smallest)
        Punto partenza = Punto::SmallestPoint(verticesSupp);
        unsigned int lato;

        //cerco il lato di partenza
        for(unsigned int i=0; i<_polygonEdges.size(); i++){

            //se trovo il lato esco dal ciclo
            if( Punto::equal(partenza, _polygonEdges[i]._from) == true) {
                lato = i;

                //condizione che mi fa uscire dal ciclo
                i=_polygonEdges.size()+1;
            }
        }
        Punto inters;
        unsigned int indietro = 0;

        do{
            all_used=0; //flag che mi indica se ho usato tutti i vertici

            //dopo l'ultimo lato andiamo al lato zero
            if(lato == _polygonEdges.size()) lato = 0;

            //flag che ci avvisa se trova una seconda intersezione nella retta
            unsigned int flagInt=0;


            //cerco se in quel lato ho intersezione
            if( _intersectionPoints.find(lato) != _intersectionPoints.end())
            {
                //salvo intersezione
                inters = _intersectionPoints.at(lato);

                //inserisco i punti della retta ordinata fino all'intersezione successiva

                //se sono alla fine della retta ,scorro indietro ->indietro=1
                if(Punto::equal(inters,sorted_end) == true || indietro == 1)
                {
                    indietro = 1;
                    auto ite = _sortedPoints.end();

                    //scorro dal max al min della retta ordinata(sempre rispetto alle x)
                    for(list<Punto>::iterator i = _sortedPoints.end(); i != _sortedPoints.begin(); i--)
                    {
                        Punto& p = *i;

                        //se il punto che trovo è un intersezione
                        if(Punto::equal(p,inters)==true)
                        {
                            //salvo l'indirizzo in ite
                            ite = i;
                            int bandiera=0;
                            int numInt = 0;

                            //finchè il numero di intersezioni è minore di due e non sono al punto iniziale della retta (bandiera=1)
                            while((bandiera!=1) && (numInt<2))
                            {
                                Punto& p = *ite;

                                //se il punto non è uno degli estremi del segmento( s1,s2),aggiungo il punto
                                if(Punto::equal(p,s1) == false && Punto::equal(p,s2) == false ){
                                    numInt++;
                                    cutPoints.push_back(p);

                                    //salvo il lato in cui mi trovo
                                    for(auto itr = _intersectionPoints.begin(); itr != _intersectionPoints.end(); itr++){
                                        if(Punto::equal(itr->second, p)==true) lato = itr->first;
                                    }
                                    flagInt = 1;
                                }else cutPoints.push_back(p); //altrimenti aggiungo s1 o s2

                                //se scorrendo all'indietro , siamo arrivati al punto iniziale , esci dal while
                                if(ite == _sortedPoints.begin()) bandiera = 1;

                                ite--;

                            }
                        }
                    }
                }else{  //SCORRO in avanti


                    auto ite = _sortedPoints.end();



                    //scorro dal min al max della retta ordinata
                    for(list<Punto>::iterator i = _sortedPoints.begin(); flagInt != 1 ; i++)
                    {
                        Punto& p = *i;

                        //se un punto è uguale all'intersezione
                        if(Punto::equal(p,inters)==true)
                        {
                            //salvo l'indirizzo in ite
                            ite = i;
                            int numInt=0;
                            //fin quando che non sono alla fine della retta e che in num di intersezioni è minore di due
                            while((ite!=_sortedPoints.end()) && (numInt<2))
                            {
                                Punto& p = *ite;

                                //se i punti non sono uguali a s1 o a s2, aggiungo
                                if(Punto::equal(p,s1) == false && Punto::equal(p,s2) == false){
                                    numInt++;
                                    cutPoints.push_back(p);

                                    //salvo il lato in cui mi trovo
                                    for(auto itr = _intersectionPoints.begin(); itr != _intersectionPoints.end(); itr++){
                                        if(Punto::equal(itr->second, p)==true) lato = itr->first;
                                    }
                                    flagInt = 1;

                                    //altrimenti aggiungi s1 o s2
                                }else cutPoints.push_back(p);

                                ite++;
                            }
                        }

                    }

                }

            }


            //se ho trovato intersezione oppure cercando il lato sono arrivata alla fine (cioè non ho trovato intersezione)
            if( _intersectionPoints.find(lato) == _intersectionPoints.end() || flagInt == 1)
            {
                //inserisco i punti _to del lato
                cutPoints.push_back(_polygonEdges[lato]._to);

                //aggiorno i vertici inseriti da verticesSup
                for(unsigned int i=0; i<verticesSupp.size(); i++){
                    if(Punto::equal(verticesSupp[i], _polygonEdges[lato]._to)==true){

                        //assegno il valore più grande in modo tale che il punto in questione non sia più il minimo(e non entri nel ciclo)
                        verticesSupp[i]=fine;
                    }
                }
            }

            //controllo di non aver inserito 2 volte _polygonEdges[lato]._t0
            int inserted_twice=0;

            //scorro la lista del poligono tagliato
            for(list<Punto>::iterator iter=cutPoints.begin(); iter!= cutPoints.end(); iter++){
                Punto& trovato = *iter;

                //se nella lista dei poligoni tagliati ho inserito due volte lo stesso vertice , incrementa inseritiduevolte
                if(Punto::equal(trovato, _polygonEdges[lato]._to)==true) inserted_twice++;
            }
            //elimina l'ultimo elemento che ho inserito due volte
            if(inserted_twice==2) cutPoints.pop_back();

            lato++;

        }while( Punto::equal(_polygonEdges[lato-1]._to , partenza) != true ); //IL DO VALE FINO A QUANDO NON RITORNO AL PRIMO PUNTO PRESO



        //Inserisco nella mappa il numero di poligoni trovati e le rispettive liste di punti
        _polygons.insert( pair<int,list<Punto>>(num_polygon,cutPoints) );

        //azzera la lista di supporto per i poligoni tagliati
        cutPoints.clear();

        //controllo se ho preso tutti i vertici (vedendo se tutti hanno valore max)
        for(unsigned int i=0; i<verticesSupp.size(); i++){
            if(Punto::equal(verticesSupp[i], fine)==true) all_used++;
        }
        num_polygon ++;

        //esco quando ho preso tutti i punti
    }while(all_used != verticesSupp.size());

    //etichetto i punti
    for(unsigned int i=1; i<num_polygon; i++){
        cutPoints = _polygons.at(i);

        //scorro la lista dei punti dei poligoni tagliati
        for(list<Punto>::iterator it = cutPoints.begin(); it!=cutPoints.end(); it++){
            Punto& p = *it;

            //scorro newPoints(vettore di tutti i punti etichettati) e inserisco
            for(unsigned int n=0; n<_newPoints.size(); n++){
                if(Punto::equal(p, _newPoints[n])==true) vertici.push_back(n);
            }
        }

        //vertici=lista di supporto dei poligoni tagliati e successivamente la resetto
        _cuttedPolygons.push_back(vertici);
        vertici.clear();
    }

    return pair<vector<Punto>, list<list<int>> >(_newPoints,_cuttedPolygons);

}

}
