#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <Eigen>

namespace Vector2DNamespace {

    class Vector2d{
      private:
        double _x;
        double _y;
      public:
        Vector2d(const double& x,
                 const double& y);
        const double& X() const { return _x; }
        const double& Y() const { return _y; }
    };
}

#endif // VECTOR2D_H
