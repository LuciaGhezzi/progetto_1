#ifndef SEGMENT_H
#define SEGMENT_H

#include "Punto.hpp"
#include "Eigen"

using namespace Eigen;
using namespace std;
using namespace PuntoNamespace;

namespace SegmentNamespace {

  class Segment {
    public:
     Punto _from, _to;
     Segment() {};
     ~Segment(){};

      Segment(const Punto& from, const Punto& to);

  };
}

#endif // SEGMENT_H
