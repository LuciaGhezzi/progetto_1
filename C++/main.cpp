#include "test_emptyclass.hpp"
#include "test_geometryfactory.hpp"
#include "test_punto.hpp"
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
