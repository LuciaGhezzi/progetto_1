#ifndef __TEST_PUNTO_H
#define __TEST_PUNTO_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>


#include "Punto.hpp"


using namespace PuntoNamespace;
using namespace testing;
using namespace std;

namespace PuntoTesting {


TEST(TestPunto,TestEqual)
{
    Punto point1 = Punto(1.2 , 2.4);
    Punto point2 = Punto(1.2 , 2.4);
    Punto point3 = Punto(1.9 , 2.6);



    try{
        EXPECT_EQ(Punto::equal(point1,point2),true);
        EXPECT_EQ(Punto::equal(point2,point3),false);

    }
    catch (const exception& exception)
    {
      FAIL();
    }
}







  //riempo rettangolo
  void FillRectanglePoints2(vector<Punto>& rectanglePoints)
  {
      rectanglePoints.push_back(Punto(1.0,1.0));
      rectanglePoints.push_back(Punto(5.0,1.0));
      rectanglePoints.push_back(Punto(5.0,3.1));
      rectanglePoints.push_back(Punto(1.0,3.1));
  }

  //riempo pentagono
  void FillPentagonPoints2(vector<Punto>& pentagonPoints)
  {
    pentagonPoints.push_back(Punto(2.5,1.0));
    pentagonPoints.push_back(Punto(4.0,2.1));
    pentagonPoints.push_back(Punto(3.4,4.2));
    pentagonPoints.push_back(Punto(1.6,4.2));
    pentagonPoints.push_back(Punto(1.0,2.1));
  }

  //riempo concavo 1
  void FillConcave1Points2(vector<Punto>& concave1Points)
  {
    concave1Points.push_back(Punto(1.5,1.0));
    concave1Points.push_back(Punto(5.6,1.5));
    concave1Points.push_back(Punto(5.5,4.8));
    concave1Points.push_back(Punto(4.0,6.2));
    concave1Points.push_back(Punto(3.2,4.2));
    concave1Points.push_back(Punto(1.0,4.0));
  }

  //riempo triangolo
  void FillTrianglePoints2(vector<Punto>& trianglePoints)
  {
    trianglePoints.push_back(Punto(0.0,1.0));
    trianglePoints.push_back(Punto(4.0,1.0));
    trianglePoints.push_back(Punto(2.0,2.0));
  }

  //riempo concavo 2
  void FillConcave2Points2(vector<Punto>& concave2Points)
  {
    concave2Points.push_back(Punto(2.0,0.0));
    concave2Points.push_back(Punto(4.0,0.0));
    concave2Points.push_back(Punto(5.0,3.0));
    concave2Points.push_back(Punto(4.0,2.0));
    concave2Points.push_back(Punto(3.0,4.0));
    concave2Points.push_back(Punto(2.0,2.0));
    concave2Points.push_back(Punto(1.0,3.0));
  }

  //testo la funzione SmallestPoint(ricerca del minimo in base all'ascissa del punto)
  TEST(TestPunto, TestSmallestPoint)
  {
      //riempo rettangolo
      vector<Punto> rectanglePoints;
      FillRectanglePoints2(rectanglePoints);
      //riempo pentagono
      vector<Punto> pentagonPoints;
      FillPentagonPoints2(pentagonPoints);
      //riempo concavo1
      vector<Punto> concave1Points;
      FillConcave1Points2(concave1Points);
      //riempo triangolo
      vector<Punto> trianglePoints;
      FillTrianglePoints2(trianglePoints);
      //riempo concavo2
      vector<Punto> concave2Points;
      FillConcave2Points2(concave2Points);

      Punto puntoRectangle;
      Punto puntoPentagon;
      Punto puntoConcave1;
      Punto puntoTriangle;
      Punto puntoConcave2;
      try{
      //test Smallestpoint rettangolo
      EXPECT_EQ(puntoRectangle.SmallestPoint(rectanglePoints)._x,1.0);
      EXPECT_EQ(puntoRectangle.SmallestPoint(rectanglePoints)._y,1.0);
      //test Smallestpoint pentagono
      EXPECT_EQ(puntoPentagon.SmallestPoint(pentagonPoints)._x,1.0);
      EXPECT_EQ(puntoPentagon.SmallestPoint(pentagonPoints)._y,2.1);
      //test Smallestpoint concavo1
      EXPECT_EQ(puntoConcave1.SmallestPoint(concave1Points)._x,1.0);
      EXPECT_EQ(puntoConcave1.SmallestPoint(concave1Points)._y,4.0);
      //test Smallestpoint triangolo
      EXPECT_EQ(puntoTriangle.SmallestPoint(trianglePoints)._x,0.0);
      EXPECT_EQ(puntoTriangle.SmallestPoint(trianglePoints)._y,1.0);
      //test Smallestpoint concavo2
      EXPECT_EQ(puntoConcave2.SmallestPoint(concave2Points)._x,1.0);
      EXPECT_EQ(puntoConcave2.SmallestPoint(concave2Points)._y,3.0);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }



  //testo la funzione BiggestPoint(ricerca del massimo in base all'ascissa del punto)
  TEST(TestPunto, TestBiggestPoint){
      //riempo rettangolo
      vector<Punto> rectanglePoints;
      FillRectanglePoints2(rectanglePoints);
      //riempo pentagono
      vector<Punto> pentagonPoints;
      FillPentagonPoints2(pentagonPoints);
      //riempo concavo1
      vector<Punto> concave1Points;
      FillConcave1Points2(concave1Points);
      //riempo triangolo
      vector<Punto> trianglePoints;
      FillTrianglePoints2(trianglePoints);
      //riempo concavo2
      vector<Punto> concave2Points;
      FillConcave2Points2(concave2Points);

      Punto puntoRectangle;
      Punto puntoPentagon;
      Punto puntoConcave1;
      Punto puntoTriangle;
      Punto puntoConcave2;
      try{
      //test BiggestPoint rettangolo
      EXPECT_EQ(puntoRectangle.BiggestPoint(rectanglePoints)._x,5.0);
      EXPECT_EQ(puntoRectangle.BiggestPoint(rectanglePoints)._y,3.1);
      //test BiggestPoint pentagono
      EXPECT_EQ(puntoPentagon.BiggestPoint(pentagonPoints)._x,4.0);
      EXPECT_EQ(puntoPentagon.BiggestPoint(pentagonPoints)._y,2.1);
      //test BiggestPoint concavo1
      EXPECT_EQ(puntoConcave1.BiggestPoint(concave1Points)._x,5.6);
      EXPECT_EQ(puntoConcave1.BiggestPoint(concave1Points)._y,1.5);
      //test BiggestPoint triangolo
      EXPECT_EQ(puntoTriangle.BiggestPoint(trianglePoints)._x,4.0);
      EXPECT_EQ(puntoTriangle.BiggestPoint(trianglePoints)._y,1.0);
      //test BiggestPoint concavo2
      EXPECT_EQ(puntoConcave2.BiggestPoint(concave2Points)._x,5.0);
      EXPECT_EQ(puntoConcave2.BiggestPoint(concave2Points)._y,3.0);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }

}
#endif // __TEST_PUNTO_H
