#ifndef __TEST_GEOMETRYFACTORY_H
#define __TEST_GEOMETRYFACTORY_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "GeometryFactory.hpp"
#include "Punto.hpp"
#include "vector"


using namespace GeometryFactoryNamespace;
using namespace testing;

namespace GeometryFactoryTesting {

//riempo rettangolo con vettori di Vector2d
    void FillRectanglePoints(vector<Vector2d>& rectanglepoints)
    {
      rectanglepoints.reserve(4);
      rectanglepoints.push_back(Vector2d(1.0,1.0));
      rectanglepoints.push_back(Vector2d(5.0,1.0));
      rectanglepoints.push_back(Vector2d(5.0,3.1));
      rectanglepoints.push_back(Vector2d(1.0,3.1));
    }

 //riempo rettangolo con vettori di Punto
    void FillRectanglePoints2(vector<Punto>& rectanglePoints)
    {
        rectanglePoints.push_back(Punto(1.0,1.0));
        rectanglePoints.push_back(Punto(5.0,1.0));
        rectanglePoints.push_back(Punto(5.0,3.1));
        rectanglePoints.push_back(Punto(1.0,3.1));
    }

  //riempo Pentagono con vettori di Vector2d
    void FillPentagonPoints(vector<Vector2d>& pentagonpoints)
    {
      pentagonpoints.reserve(5);
      pentagonpoints.push_back(Vector2d(2.5,1.0));
      pentagonpoints.push_back(Vector2d(4.0,2.1));
      pentagonpoints.push_back(Vector2d(3.4,4.2));
      pentagonpoints.push_back(Vector2d(1.6,4.2));
      pentagonpoints.push_back(Vector2d(1.0,2.1));
    }

  //riempo Pentagono con vettori di Punto
    void FillPentagonPoints2(vector<Punto>& pentagonPoints)
    {
      pentagonPoints.push_back(Punto(2.5,1.0));
      pentagonPoints.push_back(Punto(4.0,2.1));
      pentagonPoints.push_back(Punto(3.4,4.2));
      pentagonPoints.push_back(Punto(1.6,4.2));
      pentagonPoints.push_back(Punto(1.0,2.1));
    }

   //riempo Concavo1 con vettori di Vector2d
    void FillConcave1Points(vector<Vector2d>& concave1points)
    {
      concave1points.reserve(5);
      concave1points.push_back(Vector2d(1.5,1.0));
      concave1points.push_back(Vector2d(5.6,1.5));
      concave1points.push_back(Vector2d(5.5,4.8));
      concave1points.push_back(Vector2d(4.0,6.2));
      concave1points.push_back(Vector2d(3.2,4.2));
      concave1points.push_back(Vector2d(1.0,4.0));
    }

    //riempo Concavo1 con vettori di Punto
    void FillConcave1Points2(vector<Punto>& concave1Points)
    {
      concave1Points.push_back(Punto(1.5,1.0));
      concave1Points.push_back(Punto(5.6,1.5));
      concave1Points.push_back(Punto(5.5,4.8));
      concave1Points.push_back(Punto(4.0,6.2));
      concave1Points.push_back(Punto(3.2,4.2));
      concave1Points.push_back(Punto(1.0,4.0));
    }

    //riempo triangolo con vettori di Vector
    void FillTrianglePoints(vector<Vector2d>& trianglepoints)
    {
      trianglepoints.reserve(3);
      trianglepoints.push_back(Vector2d(0.0,1.0));
      trianglepoints.push_back(Vector2d(4.0,1.0));
      trianglepoints.push_back(Vector2d(2.0,2.0));
    }

    //riempo triangolo con vettori di Punti
    void FillTrianglePoints2(vector<Punto>& trianglePoints)
    {
      trianglePoints.push_back(Punto(0.0,1.0));
      trianglePoints.push_back(Punto(4.0,1.0));
      trianglePoints.push_back(Punto(2.0,2.0));
    }

    //riempo Concavo2 con vettori di Vector2d
    void FillConcave2Points(vector<Vector2d>& concave2points)
    {
      concave2points.reserve(7);
      concave2points.push_back(Vector2d(2.0,0.0));
      concave2points.push_back(Vector2d(4.0,0.0));
      concave2points.push_back(Vector2d(5.0,3.0));
      concave2points.push_back(Vector2d(4.0,2.0));
      concave2points.push_back(Vector2d(3.0,4.0));
      concave2points.push_back(Vector2d(2.0,2.0));
      concave2points.push_back(Vector2d(1.0,3.0));
    }

    //riempo Concavo1 con vettori di Punto
    void FillConcave2Points2(vector<Punto>& concave2Points)
    {
      concave2Points.push_back(Punto(2.0,0.0));
      concave2Points.push_back(Punto(4.0,0.0));
      concave2Points.push_back(Punto(5.0,3.0));
      concave2Points.push_back(Punto(4.0,2.0));
      concave2Points.push_back(Punto(3.0,4.0));
      concave2Points.push_back(Punto(2.0,2.0));
      concave2Points.push_back(Punto(1.0,3.0));
    }

    void FillFalsePoints(vector<Vector2d>& falsepoints)
    { falsepoints.reserve(2);
      falsepoints.push_back(Vector2d(0.0,0.0));
      falsepoints.push_back(Vector2d(9.0,0.0));
    }




 //Test per la creazione del poligono
  TEST(TestGeometryFactory, TestSetPolygon)
  {
    //riempo rettangolo
    vector<Vector2d> rectanglepoints;
    FillRectanglePoints(rectanglepoints);
    //riempo pentagono
    vector<Vector2d> pentagonpoints;
    FillPentagonPoints(pentagonpoints);
    //riempo concavo1
    vector<Vector2d> concave1points;
    FillPentagonPoints(concave1points);
    //riempo triangolo
    vector<Vector2d> trianglepoints;
    FillTrianglePoints(trianglepoints);
    //riempo concavo2
    vector<Vector2d> concave2points;
    FillPentagonPoints(concave2points);
    //riempo poligono falso
    vector<Vector2d> falsepoints;
    FillFalsePoints(falsepoints);


    GeometryFactory geometryFactoryRectangle;
    GeometryFactory geometryFactoryPentagon;
    GeometryFactory geometryFactoryConcave1;
    GeometryFactory geometryFactoryTriangle;
    GeometryFactory geometryFactoryConcave2;
    GeometryFactory geometryFactoryFalse;

    try
    { //test creazione rettangolo
      EXPECT_EQ(geometryFactoryRectangle.SetPolygon(rectanglepoints), true);
      //test creazione pentagono
      EXPECT_EQ(geometryFactoryPentagon.SetPolygon(pentagonpoints), true);
      //test creazione concavo1
      EXPECT_EQ(geometryFactoryConcave1.SetPolygon(concave1points), true);
      //test creazione triangolo
      EXPECT_EQ(geometryFactoryTriangle.SetPolygon(trianglepoints), true);
      //test creazione concavo2
      EXPECT_EQ(geometryFactoryConcave2.SetPolygon(concave2points), true);

    }

    catch (const exception& exception)
    {
      FAIL();
    }

    //test falso
    try
    {
     geometryFactoryFalse.SetPolygon(falsepoints);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("errore nel creare poligono"));
    }


  }


//riempo il segmento che interseca il rettangolo
  void FillRectangleSegmentPoints(vector<Vector2d>& rectanglesegmentpoints)
  {
    rectanglesegmentpoints.reserve(2);
    rectanglesegmentpoints.push_back(Vector2d(2.0,1.2));
    rectanglesegmentpoints.push_back(Vector2d(4.0,3.0));
  }

 //riempo il segmento che interseca il pentagono
  void FillPentagonSegmentPoints(vector<Vector2d>& pentagonsegmentpoints)
  {
    pentagonsegmentpoints.reserve(2);
    pentagonsegmentpoints.push_back(Vector2d(1.4,2.75));
    pentagonsegmentpoints.push_back(Vector2d(3.6,2.2));
  }

 //riempo il segmento che interseca il Concavo1
  void FillConcave1SegmentPoints(vector<Vector2d>& concave1segmentpoints)
  {
    concave1segmentpoints.reserve(2);
    concave1segmentpoints.push_back(Vector2d(2.0,3.7));
    concave1segmentpoints.push_back(Vector2d(4.1,5.9));
  }

 //riempo il segmento che interseca il Triangolo
  void FillTriangleSegmentPoints(vector<Vector2d>& trianglesegmentpoints)
  {
    trianglesegmentpoints.reserve(2);
    trianglesegmentpoints.push_back(Vector2d(1.5,1.6));
    trianglesegmentpoints.push_back(Vector2d(3.0,1.4));
  }

 //riempo il segmento che interseca il Concavo2
  void FillConcave2SegmentPoints(vector<Vector2d>& concave2segmentpoints)
  {
    concave2segmentpoints.reserve(2);
    concave2segmentpoints.push_back(Vector2d(2.5,1.2));
    concave2segmentpoints.push_back(Vector2d(3.5,2.2));
  }


//test creazione segmento
  TEST(TestGeometryFactory, TestSetSegment)
  {
    //segmento rettangolo
    vector<Vector2d> rectanglesegmentpoints;
    FillRectangleSegmentPoints(rectanglesegmentpoints);
    //segmento pentagono
    vector<Vector2d> pentagonsegmentpoints;
    FillPentagonSegmentPoints(pentagonsegmentpoints);
    //segmento concavo1
    vector<Vector2d> concave1segmentpoints;
    FillConcave1SegmentPoints(concave1segmentpoints);
    //segmento triangolo
    vector<Vector2d> trianglesegmentpoints;
    FillTriangleSegmentPoints(trianglesegmentpoints);
    //segmento concavo2
    vector<Vector2d> concave2segmentpoints;
    FillConcave2SegmentPoints(concave2segmentpoints);

    GeometryFactory geometryFactoryRectangle;
    GeometryFactory geometryFactoryPentagon;
    GeometryFactory geometryFactoryConcave1;
    GeometryFactory geometryFactoryTriangle;
    GeometryFactory geometryFactoryConcave2;


    try
    { //test creazione segmento rettangolo
      EXPECT_EQ(geometryFactoryRectangle.SetSegment(rectanglesegmentpoints), true);
      //test creazione segmento pentagono
      EXPECT_EQ(geometryFactoryPentagon.SetSegment(pentagonsegmentpoints), true);
      //test creazione segmento concavo 1
      EXPECT_EQ(geometryFactoryConcave1.SetSegment(concave1segmentpoints), true);
      //test creazione segmento triangolo
      EXPECT_EQ(geometryFactoryTriangle.SetSegment(trianglesegmentpoints), true);
      //test creazione segmento concavo 2
      EXPECT_EQ(geometryFactoryConcave2.SetSegment(concave2segmentpoints), true);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }

  //test ricerca intersezioni
  TEST(TestGeometryFactory, TestFindIntersection){
      //riempo rettangolo
      vector<Vector2d> rectanglepoints;
      FillRectanglePoints(rectanglepoints);
      vector<Vector2d> rectanglesegmentpoints;
      FillRectangleSegmentPoints(rectanglesegmentpoints);
      //riempo pentagono
      vector<Vector2d> pentagonpoints;
      FillPentagonPoints(pentagonpoints);
      vector<Vector2d> pentagonsegmentpoints;
      FillPentagonSegmentPoints(pentagonsegmentpoints);
      //riempo concavo1
      vector<Vector2d> concave1points;
      FillConcave1Points(concave1points);
      vector<Vector2d> concave1segmentpoints;
      FillConcave1SegmentPoints(concave1segmentpoints);
      //riempo triangolo
      vector<Vector2d> trianglepoints;
      FillTrianglePoints(trianglepoints);
      vector<Vector2d> trianglesegmentpoints;
      FillTriangleSegmentPoints(trianglesegmentpoints);
      //riempo concavo2
      vector<Vector2d> concave2points;
      FillConcave2Points(concave2points);
      vector<Vector2d> concave2segmentpoints;
      FillConcave2SegmentPoints(concave2segmentpoints);


      GeometryFactory geometryFactoryRectangle;
      GeometryFactory geometryFactoryPentagon;
      GeometryFactory geometryFactoryConcave1;
      GeometryFactory geometryFactoryTriangle;
      GeometryFactory geometryFactoryConcave2;

      try
      { //test intersezioni rettangolo
        geometryFactoryRectangle.SetPolygon(rectanglepoints);
        geometryFactoryRectangle.SetSegment(rectanglesegmentpoints);
        geometryFactoryRectangle.findIntersection();
        EXPECT_TRUE(geometryFactoryRectangle._intersectionPoints.at(0)._x - 1.78 < 1e-6);
        EXPECT_TRUE(geometryFactoryRectangle._intersectionPoints.at(0)._y - 1.0 < 1e-6);
        EXPECT_TRUE(geometryFactoryRectangle._intersectionPoints.at(2)._x - 4.12 < 1e-6 );
        EXPECT_TRUE(geometryFactoryRectangle._intersectionPoints.at(2)._y - 3.1 < 1e-6);
        //test intersezioni pentagono
        geometryFactoryPentagon.SetPolygon(pentagonpoints);
        geometryFactoryPentagon.SetSegment(pentagonsegmentpoints);
        geometryFactoryPentagon.findIntersection();
        EXPECT_TRUE(geometryFactoryPentagon._intersectionPoints.at(0)._x - 4.0 < 1e-6);
        EXPECT_TRUE(geometryFactoryPentagon._intersectionPoints.at(0)._y - 2.1 < 1e-6);
        EXPECT_TRUE(geometryFactoryPentagon._intersectionPoints.at(3)._x - 1.2 < 1e-6);
        EXPECT_TRUE(geometryFactoryPentagon._intersectionPoints.at(3)._y - 2.8 < 1e-6);
        //test intersezioni concavo1
        geometryFactoryConcave1.SetPolygon(concave1points);
        geometryFactoryConcave1.SetSegment(concave1segmentpoints);
        geometryFactoryConcave1.findIntersection();
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(2)._x - 4.3 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(2)._y - 6.1 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(3)._x - 3.73 < 1e-6 );
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(3)._y - 5.51 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(4)._x - 2.41 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(4)._y - 4.13 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(5)._x - 1.2 < 1e-6 );
        EXPECT_TRUE(geometryFactoryConcave1._intersectionPoints.at(5)._y - 2.86 < 1e-6);
        //test intersezioni triangolo
        geometryFactoryTriangle.SetPolygon(trianglepoints);
        geometryFactoryTriangle.SetSegment(trianglesegmentpoints);
        geometryFactoryTriangle.findIntersection();
        EXPECT_TRUE(geometryFactoryTriangle._intersectionPoints.at(1)._x - 3.28 < 1e-6);
        EXPECT_TRUE(geometryFactoryTriangle._intersectionPoints.at(1)._y - 1.37 < 1e-6);
        EXPECT_TRUE(geometryFactoryTriangle._intersectionPoints.at(2)._x - 1.27 < 1e-6);
        EXPECT_TRUE(geometryFactoryTriangle._intersectionPoints.at(2)._y - 1.64 < 1e-6 );
        //test intersezioni concavo2
        geometryFactoryConcave2.SetPolygon(concave2points);
        geometryFactoryConcave2.SetSegment(concave2segmentpoints);
        geometryFactoryConcave2.findIntersection();
        EXPECT_TRUE(geometryFactoryConcave2._intersectionPoints.at(3)._x - 3.77 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave2._intersectionPoints.at(3)._y - 2.47 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave2._intersectionPoints.at(6)._x - 1.825 < 1e-6);
        EXPECT_TRUE(geometryFactoryConcave2._intersectionPoints.at(6)._y - 0.525 < 1e-6);
}
      catch (const exception& exception)
      {
        FAIL();
      }

  }

  //test taglio poligoni
  TEST(TestGeometryFactory, TestCutPolygon){
      //rettangolo
      vector<Vector2d> rectanglesegmentpoints;
      FillRectangleSegmentPoints(rectanglesegmentpoints);
      vector<Vector2d> rectanglepoints;
      FillRectanglePoints(rectanglepoints);
      //concavo1
      vector<Vector2d> concave1points;
      FillConcave1Points(concave1points);
      vector<Vector2d> concave1segmentpoints;
      FillConcave1SegmentPoints(concave1segmentpoints);
      //pentagono
      vector<Vector2d> pentagonpoints;
      FillPentagonPoints(pentagonpoints);
      vector<Vector2d> pentagonsegmentpoints;
      FillPentagonSegmentPoints(pentagonsegmentpoints);
      //concavo2
      vector<Vector2d> concave2points;
      FillConcave2Points(concave2points);
      vector<Vector2d> concave2segmentpoints;
      FillConcave2SegmentPoints(concave2segmentpoints);
      //triangolo
      vector<Vector2d> trianglepoints;
      FillTrianglePoints(trianglepoints);
      vector<Vector2d> trianglesegmentpoints;
      FillTriangleSegmentPoints(trianglesegmentpoints);
     // pair< vector<Punto>,list<list<int>>>


      GeometryFactory geometryFactoryConcave1;
      GeometryFactory geometryFactoryRectangle;
      GeometryFactory geometryFactoryPentagon;
      GeometryFactory geometryFactoryTriangle;
      GeometryFactory geometryFactoryConcave2;

      try{
          //taglio del pentagono
          geometryFactoryPentagon.SetPolygon(pentagonpoints);
          geometryFactoryPentagon.SetSegment(pentagonsegmentpoints);
          geometryFactoryPentagon.findIntersection();
          geometryFactoryPentagon.SortPoints();
          geometryFactoryPentagon.fillNewPoints();
          vector<int> pen={0, 1, 2, 3, 4};
          geometryFactoryPentagon.CutPolygon(pentagonpoints, pentagonsegmentpoints, pen);
          list<list<int>> pent={{0, 1, 7, 6, 5, 4}, {5, 6, 7, 1, 2, 3}};
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[0], Punto(2.5,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[1], Punto(4.0,2.1)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[2], Punto(3.4,4.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[3], Punto(1.6,4.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[4], Punto(1.0,2.1)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[5], Punto(1.2,2.8)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[6], Punto(1.4,2.75)));
          EXPECT_TRUE(Punto::equal(geometryFactoryPentagon._newPoints[7], Punto(3.6,2.2)));

          EXPECT_EQ(geometryFactoryPentagon._cuttedPolygons, pent);



          //taglio del rettangolo
          geometryFactoryRectangle.SetPolygon(rectanglepoints);
          geometryFactoryRectangle.SetSegment(rectanglesegmentpoints);
          geometryFactoryRectangle.findIntersection();
          geometryFactoryRectangle.SortPoints();
          geometryFactoryRectangle.fillNewPoints();
          vector<int> rect={0, 1, 2, 3};
          geometryFactoryRectangle.CutPolygon(rectanglepoints,rectanglesegmentpoints, rect);

          list<list<int>> rett={{4, 5, 6, 7, 3, 0}, {2, 7, 6, 5, 4, 1}};
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[0], Punto(1.0,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[1], Punto(5.0,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[2], Punto(5.0,3.1)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[3], Punto(1.0,3.1)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[4], Punto(1.777777777,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[5], Punto(2.0,1.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[6], Punto(4.0,3.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryRectangle._newPoints[7], Punto(4.111111111,3.1)));
          EXPECT_EQ(geometryFactoryRectangle._cuttedPolygons, rett);


          //taglio del concavo2
          geometryFactoryConcave2.SetPolygon(concave2points);
          geometryFactoryConcave2.SetSegment(concave2segmentpoints);
          geometryFactoryConcave2.findIntersection();
          geometryFactoryConcave2.SortPoints();
          geometryFactoryConcave2.fillNewPoints();
          vector<int> con2={0, 1, 2, 3, 4, 5, 6};
          geometryFactoryConcave2.CutPolygon(concave2points, concave2segmentpoints, con2);
          list<list<int>> conc2={{7, 8, 9, 10, 4, 5, 6}, {1, 2, 3, 10, 9, 8, 7, 0}};
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[0], Punto(2.0,0.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[1], Punto(4.0,0.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[2], Punto(5.0,3.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[3], Punto(4.0,2.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[4], Punto(3.0,4.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[5], Punto(2.0,2.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[6], Punto(1.0,3.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[7], Punto(1.825,0.525)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[8], Punto(2.5,1.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[9], Punto(3.5,2.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave2._newPoints[10], Punto(3.766666666,2.466666666)));
          EXPECT_EQ(geometryFactoryConcave2._cuttedPolygons, conc2);


          //taglio del concavo1
          geometryFactoryConcave1.SetPolygon(concave1points);
          geometryFactoryConcave1.SetSegment(concave1segmentpoints);
          geometryFactoryConcave1.findIntersection();
          geometryFactoryConcave1.SortPoints();
          geometryFactoryConcave1.fillNewPoints();
          vector<int> con1={0, 1, 2, 3, 4, 5};
          geometryFactoryConcave1.CutPolygon(concave1points, concave1segmentpoints, con1);
          list<list<int>> conc1={{6, 7, 8, 5}, {1, 2, 11, 10, 9, 4, 8, 7, 6, 0}, {9, 10, 11, 3}};
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[0], Punto(1.5,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[1], Punto(5.6,1.5)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[2], Punto(5.5,4.8)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[3], Punto(4.0,6.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[4], Punto(3.2,4.2)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[5], Punto(1.0,4.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[6], Punto(1.191216216,2.852702702)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[7], Punto(2.0,3.7)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[8], Punto(2.408597285,4.128054298)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[9], Punto(3.721311475,5.503278688)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[10], Punto(4.1,5.9)));
          EXPECT_TRUE(Punto::equal(geometryFactoryConcave1._newPoints[11], Punto(4.204326923,6.009294871)));
          EXPECT_EQ(geometryFactoryConcave1._cuttedPolygons, conc1);

          //taglio del triangolo
          geometryFactoryTriangle.SetPolygon(trianglepoints);
          geometryFactoryTriangle.SetSegment(trianglesegmentpoints);
          geometryFactoryTriangle.findIntersection();
          geometryFactoryTriangle.SortPoints();
          geometryFactoryTriangle.fillNewPoints();
          vector<int> tri={0, 1, 2};
          geometryFactoryTriangle.CutPolygon(trianglepoints, trianglesegmentpoints, tri);

          list<list<int>> triang={{1, 6, 5, 4, 3, 0}, {3, 4, 5, 6, 2}};
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[0], Punto(0.0,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[1], Punto(4.0,1.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[2], Punto(2.0,2.0)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[3], Punto(1.263157894,1.631578947)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[4], Punto(1.5,1.6)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[5], Punto(3.0,1.4)));
          EXPECT_TRUE(Punto::equal(geometryFactoryTriangle._newPoints[6], Punto(3.272727272,1.363636363)));

          EXPECT_EQ(geometryFactoryTriangle._cuttedPolygons, triang);

      }
      catch (const exception& exception)
      {
        FAIL();
      }
  }
}

#endif // __TEST_GEOMETRYFACTORY_H
