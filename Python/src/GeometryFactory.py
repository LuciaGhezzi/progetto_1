from src import Point
from src import Segment
import numpy as np


#Definizione classe Vector2d(v0,v1)
class Vector2d:
    def __init__(self, x: float, y: float):
        self.v0 = x
        self.v1 = y
    def X(self):
        return self.v0
    def Y(self):
        return self.v1


#definizione GeometryFactory
class GeometryFactory:
    def __init__(self):
     self.polygonVertices = []                       #vertici del poligono
     self.polygonEdges= []                           #lati del poligono
     self.intersectionPoints = {}                    #(chiave:lato di intersezione,valore:coordinate del punto corrispondente)
     self.sortedPoints = []                          #punti della retta(s1,s2+intersezioni) ordinati per x crescente
     self.newPoints = []                             #vettore che contiene tutti i punti del poligono(vertici+ intersezioni+s1+s2)
     self.cuttedPolygons= {}                         #lista contenente gli insiemi formati dai vertici dei poligoni tagliati
     self.intersectionSegment:Segment.Segment        #segmento s1-s2

#SmallestPoint:ricerca del punto con x minima
    def smallestPoint( self, vertices: [])-> Point.Point:
        min:int=0
        for i in range(1 , len(vertices)):
           if (vertices[i].X < vertices[min].X):
               min=i
           elif (abs(vertices[i].X - vertices[min].X) < 1e-8):
               if (vertices[i].Y < vertices[min].Y):
                    min = i
        small = Point.Point(vertices[min].X,vertices[min].Y)
        return small

#BiggestPoint:ricerca del punto con x massima
    def biggestPoint( self, vertices:[])->Point.Point:
        max:int=0

        for i in range(1 , len(vertices)):
           if (vertices[i].X > vertices[max].X):
              max=i
           elif (abs(vertices[i].X - vertices[max].X) < 1e-8):
               if (vertices[i].Y > vertices[max].Y):
                       max = i

        big=Point.Point( vertices[max].X,vertices[max].Y)
        return big
#Equal: confronta l'uguaglianza di due punti
    def equal(self, p1:Point.Point, p2:Point.Point):
        if (abs(p1.X - p2.X) < 1e-8 and abs(p1.Y - p2.Y) < 1e-8):
            return True
#creo poligono
    def SetPolygon(self,vertices:[]):
      numVertices = len(vertices)                       #conto numero di vertici
      for i in range(0, numVertices):                   #riempo vettore con i vertici
          vertex:Vector2d=vertices[i]
          point=Point.Point(vertex.X(), vertex.Y())
          self.polygonVertices.append(point)

      for j in range (0, numVertices):                  #riempo vettore con i lati
          _from:Point.Point=self.polygonVertices[j]

          to:Point.Point=self.polygonVertices[(j+1) % numVertices]
          segmento=Segment.Segment(_from,to)
          self.polygonEdges.append(segmento)

      if numVertices < 3:                               #controllo che i vertici siano >=3
          raise ValueError("Errore numero vertici")

      return True



#creo segmento s1-s2
    def SetSegment(self,points:[]):
        puntoda=Vector2d(points[0].X(), points[0].Y())
        puntoper=Vector2d(points[1].X(), points[1].Y())
        puntofrom=Point.Point(puntoda.v0,puntoda.v1)
        puntoto = Point.Point(puntoper.v0, puntoper.v1)
        self.intersectionSegment=Segment.Segment(puntofrom,puntoto)

        return True

#trovo le intersezioni
    def FindIntersection(self):
        originFisrtSegment:np.zeros(2)
        righthandside:np.zeros(2)
        endfirstsegment:np.zeros(2)

        #Definisco il primo segmento
        originFisrtSegment= np.array([self.intersectionSegment.From.X, self.intersectionSegment.From.Y])
        endfirstsegment = np.array([self.intersectionSegment.To.X,self.intersectionSegment.To.Y])
        Matrixtangentvector:np.zeros((2,2))

        #scorro su tutti i lati per cercare l'intersezione
        for i in range ( 0, len(self.polygonEdges)):

            #Definisco secondo segmento
            Matrixtangentvector=np.array([[self.intersectionSegment.To.X-self.intersectionSegment.From.X ,self.polygonEdges[i].From.X-self.polygonEdges[i].To.X],
                                               [self.intersectionSegment.To.Y-self.intersectionSegment.From.Y,self.polygonEdges[i].From.Y-self.polygonEdges[i].To.Y]])
            righthandside=np.array([self.polygonEdges[i].From.X-originFisrtSegment[0],self.polygonEdges[i].From.Y-originFisrtSegment[1]])

            #calcolo intersezione: faccio inversa(matrixTangentVector)
            SolverMatrix=np.array([[Matrixtangentvector[1][1], -Matrixtangentvector[0][1]], [-Matrixtangentvector[1][0],Matrixtangentvector[0][0]]])

            #Se il lato e la traccia della matrice non sono paralleli cerco l'intersezione tramite la coordinata parametrica
            resultparametricCoordinates= SolverMatrix.dot(righthandside)

            #nell'inversa devo dividere per il determinante della matrice, lo facciamo adesso in modo da fare una sola divisione
            resultparametricCoordinates=resultparametricCoordinates/np.linalg.det(Matrixtangentvector)

            #tipologia intersezione
            if resultparametricCoordinates[1]>= 1e-8 and (resultparametricCoordinates[1]-1) <= 1e-8:
                IntersectionPoint=Point.Point(0,0)
                IntersectionPoint.X =self.polygonEdges[i].From.X+(self.polygonEdges[i].To.X-self.polygonEdges[i].From.X)*resultparametricCoordinates[1]
                IntersectionPoint.Y= self.polygonEdges[i].From.Y + (self.polygonEdges[i].To.Y - self.polygonEdges[i].From.Y)*resultparametricCoordinates[1]
                self.intersectionPoints[i]=IntersectionPoint

                #se è un vertice eliminalo
                if self.equal(IntersectionPoint ,self.polygonEdges[i].From)==True or self.equal(IntersectionPoint,self.polygonEdges[i].To)==True:

                    #controllo se c'è un vertice tra le intersezioni per tutti i lati
                    if (i >=1 and self.equal(self.intersectionPoints[i], self.intersectionPoints[i-1])==True):
                         self.intersectionPoints.pop(i)
                    if (i==(len(self.polygonEdges)-1) and self.equal(self.intersectionPoints[i],self.intersectionPoints[0])==True):
                         self.intersectionPoints.pop(i)

   #ordino i punti per x crescente
    def SortPoints(self):

       #creo vettore di supporto di dimensione (intersezioni+s1+s2) e inserisco punto from e punto to del segmento
       punti=[]
       punti.append(self.intersectionSegment.From)
       punti.append(self.intersectionSegment.To)

       #scorro la lista e inserisco tutti i punti di intersezione
       for  k in self.intersectionPoints.__iter__():
           punti.append(self.intersectionPoints[k])

       #ordino il vettore "punti"(vettore di supporto)tramite Smallestpoint
       for j in range(0, len(punti)):
            min=self.smallestPoint(punti)
            self.sortedPoints.append(min)

            #tolgo i vertici inseriti da punti
            for i in range (0, len(punti)):
            #assegno il massimo dei punti(rispetto a x) tramite Biggest
              if self.equal(punti[i],min):
                   punti[i] = self.biggestPoint(punti)
       return True

#riempe il vettore di tutti i punti(newpoints)
    def FillNewPoints(self):
        i=0

        #aggiungo vertici
        while i<len(self.polygonVertices):
            self.newPoints.append(self.polygonVertices[i])
            i+=1

        # scorro la retta ordinata e inserisco tutti i punti
        for iter in range(0, len(self.sortedPoints)):
            point:Point.Point
            point=self.sortedPoints[iter]
            flag=0

            #controllo che il punto non sia un vertice
            for j in range(0, len(self.polygonVertices)):
                if self.equal(point,self.polygonVertices[j])==True:
                    flag=1
            #se non è vertice lo inserisco
            if flag==0:
                self.newPoints.append(point)

#funzione che taglia il poligono
    def CutPolygon(self, points:[], segment:[]):
        all_used=0
        seg1:Vector2d=segment[0]
        seg2: Vector2d = segment[1]

        #salvo s1, s2 (estremi del  segmento) in due variabili
        s1=Point.Point(seg1.v0, seg1.v1)
        s2=Point.Point(seg2.v0, seg2.v1)

        #salvo l'ultimo punto della retta ordinata
        sorted_end = self.sortedPoints[-1]

        #lista di supporto dei vertici
        verticessup=[]

        #lista di supporto dei poligoni tagliati ad ogni giro
        polygons= {}
        for i in range (0,len(self.polygonVertices)):
          polygons[i]=[]


        #riempo la lista di supporto dei vertici
        for i in range(0, len(points)):
            vertex:Vector2d=points[i]
            punto=Point.Point(vertex.v0, vertex.v1)
            verticessup.append(punto)

        #prendo il punto con x massima tramite Biggest
        fine =self.biggestPoint(verticessup)
        flagdeldo=0
        num_polygon:int=1
        while flagdeldo == 0:
            #parto dal vertice con x più piccola e la inserisco
            partenza: Point.Point
            partenza = self.smallestPoint(verticessup)
            lato = 0

            #cerco il lato di partenza
            for i in range(0, len(self.polygonEdges)):

                #se trovo il lato esco dal ciclo
                if self.equal(partenza,self.polygonEdges[i].From)==True:
                    lato=i

                    #condizione che mi fa uscire dal ciclo
                    i=len(self.polygonEdges)+1
            inters:Point.Point
            indietro=0
            flagdeldo2=0
            while flagdeldo2 == 0:
                all_used=0                          #flag che mi indica se ho usato tutti i vertici

                if lato ==len(self.polygonEdges):   #dopo l'ultimo lato andiamo al lato zero
                    lato=0
                flagint=0                           #flag che ci avvisa se trova una seconda intersezione nella retta


                #cerco se in quel lato ho intersezione o meno
                #altrimenti (se non ne ho) aggiungo il punto to del lato
                if lato in self.intersectionPoints:

                    #salvo intersezione
                  inters = self.intersectionPoints[lato]

                  #inserisco i punti del segmento che taglia fino all'intersezione successiva
                  #se sono alla fine della retta ,scorro indietro ->indietro=1
                  if self.equal(inters, sorted_end) is True or indietro == 1:
                    indietro=1
                  #scorro dal max al min della retta ordinata(sempre rispetto ad ascissa crescente)
                    for i in range (len(self.sortedPoints)-1, -1, -1):

                  #se il punto che trovo è un intersezione
                        if self.equal(self.sortedPoints[i],inters) is True:

                            #salvo l'indirizzo in ite
                            ite=i
                            bandiera=0
                            num_int=0

                            #finchè il numero di intersezioni è minore di due e non sono al punto iniziale della retta (bandiera=1)
                            while bandiera!=1 and num_int<2:
                                p2=self.sortedPoints[ite]

                                #se il punto non è uno degli estremi del segmento( s1,s2),aggiungo il punto
                                if not self.equal(p2,s1) and not self.equal(p2,s2):
                                    num_int+=1
                                    polygons[num_polygon].append(p2)
                                        #devo salvare il lato in cui mi trovo
                                        #sto scorrendo la mappa sui valori tramite itervalues()
                                    for j,k in  self.intersectionPoints.items():
                                        if self.equal(k,p2) is True:
                                             lato = j
                                    flagint=1

                                #altrimenti aggiungo s1 o s2
                                else:
                                      polygons[num_polygon].append(p2)
                                #se scorrendo all'indietro , siamo arrivati al punto iniziale , esci dal while
                                if self.equal(self.sortedPoints[ite],self.sortedPoints[0]):
                                    bandiera=1
                                ite-=1


                #se non sono ancora alla fine della retta, scorro in avanti
                  else:
                #scorro dal min al max della retta ordinata
                        for i in range (0,len(self.sortedPoints)):

                #se un punto è uguale all'intersezione
                            if self.equal(self.sortedPoints[i], inters) is True:
                #salvo l'indirizzo in ite
                                ite=i
                                num_int=0
                #fin quando che non sono alla fine della retta e che il num di intersezioni è minore di due
                                while ite!=len(self.sortedPoints) and num_int < 2:
                                    p2=self.sortedPoints[ite]

                #se i punti non sono uguali a s1 o a s2, aggiungo
                                    if not self.equal(p2,s1) and not self.equal(p2,s2) :
                                        num_int+=1

                                        polygons[num_polygon].append(p2)

                #devo salvare il lato in cui mi trovo sto scorrendo la mappa sui valori e sulle chiavi tramite items()
                #prendo in considerazione il valore salvato dunque in k, e salvo chiave j
                                        for j, k in self.intersectionPoints.items():
                                            if self.equal(k,p2) is True:
                                                lato = j
                                        flagint=1

                #altrimenti aggiungi s1 o s2
                                    else:
                                        polygons[num_polygon].append(p2)
                                    ite+=1

                #se ho trovato intersezione oppure cercando il lato sono arrivata alla fine
                #(cioè non ho trovato intersezione)
                if lato not in self.intersectionPoints or flagint == 1:

                #inserisco i punti _to del lato
                    p11=Point.Point(self.polygonEdges[lato].To.X,self.polygonEdges[lato].To.Y)
                    polygons[num_polygon].append(p11)

                #aggiorno i vertici inseriti da verticesSup
                    for i in range(0,len(verticessup)):
                        if self.equal(verticessup[i], self.polygonEdges[lato].To) is True:

                #assegno il valore più grande in modo che il punto non sia più il minimo(e non entri nel ciclo)
                            verticessup[i]=self.biggestPoint(verticessup)

                ##controllo di non aver inserito 2 volte _polygonEdges[lato]._t0

                inserted_twice=0

                #scorro la lista del poligono tagliato
                for i in range(0,len(polygons[num_polygon])):

                #se nella lista dei poligoni tagliati ho inserito due volte lo stesso vertice ,incrementa inserted_twice
                    if self.equal(polygons[num_polygon][i],self.polygonEdges[lato].To) is True:
                        inserted_twice+=1

                #elimina l'ultimo elemento che ho inserito due volte
                if inserted_twice==2:
                    polygons[num_polygon].pop()
                lato+=1

                #Il while vale fino a quando non ritorno al primo punto preso
                if self.equal(self.polygonEdges[lato-1].To,partenza) is True:
                  flagdeldo2=1

               #controllo se ho preso tutti i vertici
            for i in range (0 ,len(verticessup)):
                if self.equal(verticessup[i],fine) is True:
                    all_used+=1
            num_polygon+=1
            # esco quando ho preso tutti i punti
            if all_used == len(verticessup):
                flagdeldo=1

        # Inserisco nella mappa con chiave il numero del poligono e le rispettive liste di punti
        for i in range (1,num_polygon):
            self.cuttedPolygons[i]=[]
        for i in range(1, num_polygon):
            cutpoints:[]
            cutpoints=polygons[i]
            # scorro la lista dei punti dei poligoni tagliati
            for j in range(0, len(cutpoints)):
                # ricerco i punti trovati nel contenitore dove li avevo nominati (NewPoints)
                for k in range (0, len(self.newPoints)):
                    if self.equal(cutpoints[j],self.newPoints[k]) is True:
                        self.cuttedPolygons[i].append(k)
        return self.newPoints,self.cuttedPolygons
