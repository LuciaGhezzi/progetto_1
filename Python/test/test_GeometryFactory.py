from unittest import TestCase
import src.GeometryFactory as geometryfactory
import src.Point as point
import src.Segment as Segment

class TestGeometryFactory(TestCase):
    def FillPoints(self)->[]:
        vertices=[geometryfactory.Point.Point(1.0, 1.0),
                  geometryfactory.Point.Point(5.0, 1.0),
                  geometryfactory.Point.Point(5.0, 3.1),
                  geometryfactory.Point.Point(1.0, 3.1)]
        return vertices

    def test_smallest_point(self):
        vertices= TestGeometryFactory.FillPoints(self)
        gf = geometryfactory.GeometryFactory()
        try:
            gf.smallestPoint(vertices)
            self.assertEqual(gf.smallestPoint(vertices).X, 1.0)
            self.assertEqual(gf.smallestPoint(vertices).Y, 1.0)
        except Exception as ex:
            self.fail()

    def test_biggest_point(self):
        vertices= TestGeometryFactory.FillPoints(self)
        gf = geometryfactory.GeometryFactory()
        try:
            self.assertEqual(gf.biggestPoint(vertices).X, 5.0)
            self.assertEqual(gf.biggestPoint(vertices).Y, 3.1)
        except Exception as ex:
            self.fail()


    def FillRectanglePoints(self) -> []:
        vertices = [geometryfactory.Vector2d(1.0, 1.0),
                    geometryfactory.Vector2d(5.0, 1.0),
                    geometryfactory.Vector2d(5.0, 3.1),
                    geometryfactory.Vector2d(1.0, 3.1)]
        return vertices


    def FillPentagonPoints(self) -> []:
        vertices = [geometryfactory.Vector2d(2.5, 1.0),
                    geometryfactory.Vector2d(4.0, 2.1),
                    geometryfactory.Vector2d(3.4, 4.2),
                    geometryfactory.Vector2d(1.6, 4.2),
                    geometryfactory.Vector2d(1.0, 2.1)]
        return vertices

    def FillConcave1Points(self) -> []:
        vertices = [geometryfactory.Vector2d(1.5, 1.0),
                    geometryfactory.Vector2d(5.6, 1.5),
                    geometryfactory.Vector2d(5.5, 4.8),
                    geometryfactory.Vector2d(4.0, 6.2),
                    geometryfactory.Vector2d(3.2, 4.2),
                    geometryfactory.Vector2d(1.0, 4.0)]
        return vertices

    def FillTrianglePoints(self) -> []:
        vertices = [geometryfactory.Vector2d(0.0, 1.0),
                    geometryfactory.Vector2d(4.0, 1.0),
                    geometryfactory.Vector2d(2.0, 2.0)]
        return vertices

    def FillConcave2Points(self) -> []:
        vertices = [geometryfactory.Vector2d(2.0, 0.0),
                    geometryfactory.Vector2d(4.0, 0.0),
                    geometryfactory.Vector2d(5.0, 3.0),
                    geometryfactory.Vector2d(4.0, 2.0),
                    geometryfactory.Vector2d(3.0, 4.0),
                    geometryfactory.Vector2d(2.0, 2.0),
                    geometryfactory.Vector2d(1.0, 3.0)]
        return vertices
    def FillFalsePolygon(self)->[]:
        vertices = [geometryfactory.Vector2d(2.0, 0.0),
                    geometryfactory.Vector2d(4.0, 0.0)]
        return vertices
    def FillSegmentRett(self) -> []:
        vertices = [geometryfactory.Vector2d(2.0, 1.2),
                    geometryfactory.Vector2d(4.0, 3.0)]
        return vertices

    def FillSegmentPent(self) -> []:
        vertices = [geometryfactory.Vector2d(1.4, 2.75),
                    geometryfactory.Vector2d(3.6, 2.2)]
        return vertices


    def FillSegmenCon1(self) -> []:
        vertices = [geometryfactory.Vector2d(2.0, 3.7),
                    geometryfactory.Vector2d(4.1, 5.9)]
        return vertices

    def FillSegmentCon2(self) -> []:
        vertices = [geometryfactory.Vector2d(2.5, 1.2),
                    geometryfactory.Vector2d(3.5, 2.2)]
        return vertices

    def FillSegmentTri(self) -> []:
        vertices = [geometryfactory.Vector2d(1.5, 1.6),
                    geometryfactory.Vector2d(3.0, 1.4)]
        return vertices

    def test_SetPolygon(self):
        verticesRett= TestGeometryFactory.FillRectanglePoints(self)
        verticesPent= TestGeometryFactory.FillPentagonPoints(self)
        verticesCon1 = TestGeometryFactory.FillConcave1Points(self)
        verticesCon2 = TestGeometryFactory.FillConcave2Points(self)
        verticesTri = TestGeometryFactory.FillTrianglePoints(self)
        verticesFalse= TestGeometryFactory.FillFalsePolygon(self)
        gf = geometryfactory.GeometryFactory()
        try:

            gf.SetPolygon(verticesRett)
            self.assertEqual(gf.SetPolygon(verticesRett), True)

        except Exception as ex:
            self.fail()

        try:
            gf.SetPolygon(verticesPent)
            self.assertEqual(gf.SetPolygon(verticesPent), True)
        except Exception as ex:
            self.fail()

        try:
            gf.SetPolygon(verticesCon1)
            self.assertEqual(gf.SetPolygon(verticesCon1), True)
        except Exception as ex:
            self.fail()

        try:
            gf.SetPolygon(verticesCon2)
            self.assertEqual(gf.SetPolygon(verticesCon2), True)
        except Exception as ex:
             self.fail()

        try:
            gf.SetPolygon(verticesTri)
            self.assertEqual(gf.SetPolygon(verticesTri), True)
        except Exception as ex:
             self.fail()

        try:
            gf.SetPolygon(verticesFalse)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Errore numero vertici")

    def test_FindIntersection(self):
        verticesRett= TestGeometryFactory.FillRectanglePoints(self)
        verticesPent= TestGeometryFactory.FillPentagonPoints(self)
        verticesCon1 = TestGeometryFactory.FillConcave1Points(self)
        verticesCon2 = TestGeometryFactory.FillConcave2Points(self)
        verticesTri = TestGeometryFactory.FillTrianglePoints(self)
        segmentRett= TestGeometryFactory.FillSegmentRett(self)
        segmentPent= TestGeometryFactory.FillSegmentPent(self)
        segmentCon1= TestGeometryFactory.FillSegmenCon1(self)
        segmentCon2=TestGeometryFactory.FillSegmentCon2(self)
        segmentTri=TestGeometryFactory.FillSegmentTri(self)
        gfr = geometryfactory.GeometryFactory()
        gfp = geometryfactory.GeometryFactory()
        gfc1 = geometryfactory.GeometryFactory()
        gfc2 = geometryfactory.GeometryFactory()
        gft = geometryfactory.GeometryFactory()




        try:
            gfr.SetPolygon(verticesRett)
            gfr.SetSegment(segmentRett)
            gfr.FindIntersection()
            self.assertTrue(gfr.intersectionPoints[0].X-1.78 < 1e-6)
            self.assertTrue(gfr.intersectionPoints[0].Y - 1.0 < 1e-6)
            self.assertTrue(gfr.intersectionPoints[2].X - 4.12 < 1e-6)
            self.assertTrue(gfr.intersectionPoints[2].Y - 3.1 < 1e-6)

        except Exception as ex:
            self.fail()

        try:
            gfp.SetPolygon(verticesPent)
            gfp.SetSegment(segmentPent)
            gfp.FindIntersection()
            self.assertTrue(gfp.intersectionPoints[0].X - 4.0 < 1e-6)
            self.assertTrue(gfp.intersectionPoints[0].Y - 2.1 < 1e-6)
            self.assertTrue(gfp.intersectionPoints[3].X - 1.2 < 1e-6)
            self.assertTrue(gfp.intersectionPoints[3].Y - 2.8 < 1e-6)

        except Exception as ex:
            self.fail()

        try:
            gfc1.SetPolygon(verticesCon1)
            gfc1.SetSegment(segmentCon1)
            gfc1.FindIntersection()
            self.assertTrue(gfc1.intersectionPoints[2].X - 4.3 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[2].Y - 6.1 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[3].X - 3.73 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[3].Y - 5.51 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[4].X - 2.41 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[4].Y - 4.13 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[5].X - 1.2 < 1e-6)
            self.assertTrue(gfc1.intersectionPoints[5].Y - 2.86 < 1e-6)

        except Exception as ex:
            self.fail()



        try:
            gfc2.SetPolygon(verticesCon2)
            gfc2.SetSegment(segmentCon2)
            gfc2.FindIntersection()
            self.assertTrue(gfc2.intersectionPoints[3].X - 3.77 < 1e-6)
            self.assertTrue(gfc2.intersectionPoints[3].Y - 2.47< 1e-6)
            self.assertTrue(gfc2.intersectionPoints[6].X - 1.825 < 1e-6)
            self.assertTrue(gfc2.intersectionPoints[6].Y - 0.525< 1e-6)
        except Exception as ex:
            self.fail()


        try:
            gft.SetPolygon(verticesTri)
            gft.SetSegment(segmentTri)
            gft.FindIntersection()
            self.assertTrue(gft.intersectionPoints[1].X - 3.28 < 1e-6)
            self.assertTrue(gft.intersectionPoints[1].Y - 1.37 < 1e-6)
            self.assertTrue(gft.intersectionPoints[2].X - 1.27 < 1e-6)
            self.assertTrue(gft.intersectionPoints[2].Y - 1.64 < 1e-6)
        except Exception as ex:
            self.fail()


    def test_CutPolygon(self):
        verticesRett= TestGeometryFactory.FillRectanglePoints(self)
        verticesPent= TestGeometryFactory.FillPentagonPoints(self)
        verticesCon1 = TestGeometryFactory.FillConcave1Points(self)
        verticesCon2 = TestGeometryFactory.FillConcave2Points(self)
        verticesTri = TestGeometryFactory.FillTrianglePoints(self)
        segmentRett= TestGeometryFactory.FillSegmentRett(self)
        segmentPent= TestGeometryFactory.FillSegmentPent(self)
        segmentCon1= TestGeometryFactory.FillSegmenCon1(self)
        segmentCon2=TestGeometryFactory.FillSegmentCon2(self)
        segmentTri=TestGeometryFactory.FillSegmentTri(self)
        gfr = geometryfactory.GeometryFactory()
        gfp = geometryfactory.GeometryFactory()
        gfc1 = geometryfactory.GeometryFactory()
        gfc2 = geometryfactory.GeometryFactory()
        gft = geometryfactory.GeometryFactory()

        try:
            gfr.SetPolygon(verticesRett)
            gfr.SetSegment(segmentRett)
            gfr.FindIntersection()
            gfr.SortPoints()
            gfr.FillNewPoints()
            gfr.CutPolygon(verticesRett,segmentRett)
            rett= {1:[4, 5, 6, 7, 3, 0],2:[2, 7, 6, 5, 4, 1]}
            self.assertTrue(gfr.equal(gfr.newPoints[0], geometryfactory.Point.Point(1.0,1.0)))
            self.assertTrue(gfr.equal(gfr.newPoints[1], geometryfactory.Point.Point(5.0, 1.0)))
            self.assertTrue(gfr.equal(gfr.newPoints[2], geometryfactory.Point.Point(5.0, 3.1)))
            self.assertTrue(gfr.equal(gfr.newPoints[3], geometryfactory.Point.Point(1.0, 3.1)))
            self.assertTrue(gfr.equal(gfr.newPoints[4], geometryfactory.Point.Point(1.777777777, 1.0)))
            self.assertTrue(gfr.equal(gfr.newPoints[5], geometryfactory.Point.Point(2.0, 1.2)))
            self.assertTrue(gfr.equal(gfr.newPoints[6], geometryfactory.Point.Point(4.0, 3.0)))
            self.assertTrue(gfr.equal(gfr.newPoints[7], geometryfactory.Point.Point(4.111111111, 3.1)))
            self.assertEqual(gfr.cuttedPolygons,rett)
        except Exception as ex:
            self.fail()


        try:
            gfp.SetPolygon(verticesPent)
            gfp.SetSegment(segmentPent)
            gfp.FindIntersection()
            gfp.SortPoints()
            gfp.FillNewPoints()
            gfp.CutPolygon(verticesPent,segmentPent)
            pent= {1: [0, 1, 7, 6, 5, 4], 2: [5, 6, 7, 1, 2, 3]}
            self.assertTrue(gfp.equal(gfp.newPoints[0], geometryfactory.Point.Point(2.5, 1.0)))
            self.assertTrue(gfp.equal(gfp.newPoints[1], geometryfactory.Point.Point(4.0, 2.1)))
            self.assertTrue(gfp.equal(gfp.newPoints[2], geometryfactory.Point.Point(3.4, 4.2)))
            self.assertTrue(gfp.equal(gfp.newPoints[3], geometryfactory.Point.Point(1.6, 4.2)))
            self.assertTrue(gfp.equal(gfp.newPoints[4], geometryfactory.Point.Point(1.0, 2.1)))
            self.assertTrue(gfp.equal(gfp.newPoints[5], geometryfactory.Point.Point(1.2, 2.8)))
            self.assertTrue(gfp.equal(gfp.newPoints[6], geometryfactory.Point.Point(1.4, 2.75)))
            self.assertTrue(gfp.equal(gfp.newPoints[7], geometryfactory.Point.Point(3.6, 2.2)))
            self.assertEqual(gfp.cuttedPolygons,pent)
        except Exception as ex:
            self.fail()

        try:
            gfc1.SetPolygon(verticesCon1)
            gfc1.SetSegment(segmentCon1)
            gfc1.FindIntersection()
            gfc1.SortPoints()
            gfc1.FillNewPoints()
            gfc1.CutPolygon(verticesCon1, segmentCon1)
            Con1 = {1: [6, 7, 8, 5], 2:[1, 2, 11, 10, 9, 4, 8, 7, 6, 0], 3:[9, 10, 11, 3]}
            self.assertTrue(gfc1.equal(gfc1.newPoints[0], geometryfactory.Point.Point(1.5, 1.0)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[1], geometryfactory.Point.Point(5.6, 1.5)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[2], geometryfactory.Point.Point(5.5, 4.8)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[3], geometryfactory.Point.Point(4.0, 6.2)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[4], geometryfactory.Point.Point(3.2, 4.2)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[5], geometryfactory.Point.Point(1.0, 4.0)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[6], geometryfactory.Point.Point(1.191216216, 2.852702702)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[7], geometryfactory.Point.Point(2.0,3.7)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[8], geometryfactory.Point.Point(2.408597285, 4.128054298)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[9], geometryfactory.Point.Point(3.721311475, 5.503278688)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[10], geometryfactory.Point.Point(4.1, 5.9)))
            self.assertTrue(gfc1.equal(gfc1.newPoints[11], geometryfactory.Point.Point(4.204326923, 6.009294871)))
            self.assertEqual(gfc1.cuttedPolygons, Con1)
        except Exception as ex:
            self.fail()



        try:
            gft.SetPolygon(verticesTri)
            gft.SetSegment(segmentTri)
            gft.FindIntersection()
            gft.SortPoints()
            gft.FillNewPoints()
            gft.CutPolygon(verticesTri, segmentTri)
            Tri = {1: [1, 6, 5, 4, 3, 0], 2: [3, 4, 5, 6, 2]}
            self.assertTrue(gft.equal(gft.newPoints[0], geometryfactory.Point.Point(0.0, 1.0)))
            self.assertTrue(gft.equal(gft.newPoints[1], geometryfactory.Point.Point(4.0, 1.0)))
            self.assertTrue(gft.equal(gft.newPoints[2], geometryfactory.Point.Point(2.0, 2.0)))
            self.assertTrue(gft.equal(gft.newPoints[3], geometryfactory.Point.Point(1.263157894, 1.631578947)))
            self.assertTrue(gft.equal(gft.newPoints[4], geometryfactory.Point.Point(1.5, 1.6)))
            self.assertTrue(gft.equal(gft.newPoints[5], geometryfactory.Point.Point(3.0, 1.4)))
            self.assertTrue(gft.equal(gft.newPoints[6], geometryfactory.Point.Point(3.272727272, 1.363636363)))
            self.assertEqual(gft.cuttedPolygons, Tri)
        except Exception as ex:
            self.fail()


        try:
            gfc2.SetPolygon(verticesCon2)
            gfc2.SetSegment(segmentCon2)
            gfc2.FindIntersection()
            gfc2.SortPoints()
            gfc2.FillNewPoints()
            gfc2.CutPolygon(verticesCon2, segmentCon2)
            Con2 = {1: [7, 8, 9, 10, 4, 5, 6], 2: [1, 2, 3, 10, 9, 8, 7, 0]}
            self.assertTrue(gfc2.equal(gfc2.newPoints[0], geometryfactory.Point.Point(2.0, 0.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[1], geometryfactory.Point.Point(4.0, 0.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[2], geometryfactory.Point.Point(5.0, 3.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[3], geometryfactory.Point.Point(4.0, 2.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[4], geometryfactory.Point.Point(3.0, 4.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[5], geometryfactory.Point.Point(2.0, 2.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[6], geometryfactory.Point.Point(1.0, 3.0)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[7], geometryfactory.Point.Point(1.825, 0.525)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[8], geometryfactory.Point.Point(2.5, 1.2)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[9], geometryfactory.Point.Point(3.5, 2.2)))
            self.assertTrue(gfc2.equal(gfc2.newPoints[10], geometryfactory.Point.Point(3.766666666, 2.466666666)))
            self.assertEqual(gfc2.cuttedPolygons, Con2)
        except Exception as ex:
            self.fail()
