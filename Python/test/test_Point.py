from unittest import TestCase
import src.Point as Point

class TestPunto(TestCase):
    def FillRectanglePoints(self)->[]:
        vertices= [Point.Point(1.0, 1.0),
                  Point.Point(5.0, 1.0),
                  Point.Point(5.0, 3.1),
                  Point.Point(1.0, 3.1)]
        return vertices

    def test_smallest_point(self):
        vertices= TestPunto.FillRectanglePoints()

        punto= Point.Point()

        try:

            self.assertEqual(punto.smallestPoint(vertices).X, 1.0)
            self.assertEqual(punto.smallestPoint(vertices).Y, 1.0)
        except Exception as ex:
            self.fail()

    def test_biggest_point(self):
        vertices= TestPunto.FillRectanglePoints()
        punto = Point.Point()
        try:
            self.assertEqual(punto.biggestPoint(vertices).X, 5.0)
            self.assertEqual(punto.biggestPoint(vertices).Y, 3.1)
        except Exception as ex:
            self.fail()